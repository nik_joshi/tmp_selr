# Named Entity Recognition

import spacy
import pandas as pd
from NER import nerd
import csv


fp = "/home/stark/PycharmProjects/tmp_selr/Preprocessing/Preprocessed/Cricket_pre.csv"

tweetList = []
with open(fp, 'rt', encoding='utf-8') as data_in:
    csv_reader = csv.reader(data_in, delimiter=',')
    for line in csv_reader:
        tweet = line[1]
        tweetList.append(tweet)

with open('cricket.txt', 'w') as fw:
    for item in tweetList:
        fw.write("%s,\n" % item)

'''
nerd_api_key = '4np3ejk2b4j11115e1nsd417fi4cfpma'
#text = ('/home/stark/PycharmProjects/tmp_selr/NER/test.txt')
text = open('/home/stark/PycharmProjects/tmp_selr/NER/cricket.txt', 'r').read()
timeout = 7000
n = nerd.NERD('nerd.eurecom.fr',nerd_api_key)
n.extract(text, 'combined', timeout)
print(n)
'''
"""from spacy import displacy
from collections import Counter
import en_core_web_sm
nlp = en_core_web_sm.load()

doc = nlp('European authorities fined Google a record $5.1 billion on Wednesday for abusing its power in the mobile phone market and ordered the company to alter its practices')
print([(X.text, X.label_) for X in doc.ents])
"""
'''
#python -m spacy download en

df = pd.read_csv('/home/stark/PycharmProjects/tmp_selr/Preprocessing/Labelled/Sports_Lab.csv')
tweets = df['Tweet_Text']
nlp = spacy.load('en')
for tweet in tweets:
    text = nlp(tweet)
    for word in text.ents:
        print(word.text, word.label_)

'''
'''
nerd4python
===========

It is a python library which provides an interface to NERD http://nerd.eurecom.fr.

#### How to use the library

    from nerd import NERD
    text = read_your_text_file()
    timeout = set_timeout_seconds()
    n = NERD ('nerd.eurecom.fr', YOUR_API_KEY)
    n.extract(text, 'combined', timeout)



'''