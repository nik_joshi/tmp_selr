# NERD API Parser

import spacy
import pandas as pd
from NER import nerd
import json
import cred

# performing Named Entities Recognition and Replacement from Ontology
def NERDParser():
    df = pd.read_csv("/home/stark/PycharmProjects/tmp_selr/NER/ChunkedCSVs/chunk1_Sports.csv", index_col=False)
    tweet = df['Tweet_Text']
    timeout = 7000
    print("Establishing connection ...!!!")
    n = nerd.NERD('nerd.eurecom.fr', cred.NERD_API_KEY)
    print("Connection Established ..!"+"\n Parsing NERD")
    json_ner = n.extract(tweet, 'combined', timeout)
    print("Parsing Successful.. Saving data to local")
    json_data_local = json.dumps(json_ner)

    # writing parsed data to JSON
    with open("/home/stark/PycharmProjects/tmp_selr/NER/ChunkedCSVs/json/NER_chunk_1_Sports.json", "w") as f:
        f.write(json_data_local)
        f.close()

    # loading parsed data
    with open('/home/stark/PycharmProjects/tmp_selr/NER/ChunkedCSVs/NER_chunk_1_Sports.json') as json_data:
        data = json.load(json_data)

    # iterating data
    for line in data:
        print("Original text " + line['label'])
        print("replaced text" + line['extractorType'])

        # replacing original tweet with respective categories
        df.Tweet_Text = pd.Series(df.Tweet_Text).str.replace(line['label'], line['extractorType'])

    # writing back to data set
    df.to_csv("/home/stark/PycharmProjects/tmp_selr/NER/NERDAppliedDataSet/chunk1_Sports_replaced.csv", encoding='utf-8', index=False)
    '''
    for d in json_NER:
        label = d['label']
        replacedEntity = d['extractorType']
        #tweetReplaced =  re.sub(label,replacedEntity,tweet)
        df['Tweet_Text'].replace(label,replacedEntity)
    print(df['Tweet_Text'])
    '''


if __name__ == "__main__":
    # function calls to parse NERD
    NERDParser()


#python -m spacy download en

'''
df = pd.read_csv('/home/stark/PycharmProjects/tmp_selr/Preprocessing/Labelled/Sports_Lab.csv')
tweets = df['Tweet_Text']
nlp = spacy.load('en')
for tweet in tweets:
    text = nlp(tweet)
    for word in text.ents:
        print(word.text, word.label_)
        
'''
