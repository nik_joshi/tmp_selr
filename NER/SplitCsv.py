import pandas as pd

def splitCSV():
    for i,chunk in enumerate(pd.read_csv('/home/stark/PycharmProjects/tmp_selr/Preprocessing/Labelled/Sports_Lab.csv', chunksize=200)):
        chunk.to_csv('/home/stark/PycharmProjects/tmp_selr/NER/ChunkedCSVs/chunk{}_Sports.csv'.format(i) , index= False)

if __name__ == "__main__":
    # function call to split the csv
    splitCSV()