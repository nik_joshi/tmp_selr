#importing libraries to crawl the data from twitter and store the same into json
import tweepy
import cred
import re
import pandas as pd

def get_all_tweets(screen_name):

    # authorizing twitter user and initialize tweepy with secret keys of user
    auth = tweepy.OAuthHandler(cred.CONSUMER_KEY, cred.CONSUMER_SECRET)
    auth.set_access_token(cred.ACCESS_TOKEN, cred.ACCESS_TOKEN_SECRET)
    api = tweepy.API(auth)

    # initializing a list to save all the tweepy Tweets
    alltweets = []

    # initial request for most recent tweets (200 is the maximum allowed count)
    new_tweets = api.user_timeline(screen_name=screen_name, count=200)

    # saving most recent tweets
    alltweets.extend(new_tweets)

    # saving the id of the oldest tweet less one
    oldest = alltweets[-1].id - 1

    # extracting the tweets
    while len(new_tweets) > 0:
        # all subsiquent requests use the max_id param to prevent duplicates
        new_tweets = api.user_timeline(screen_name=screen_name, count=200, max_id=oldest)

        # saving most recent tweets
        alltweets.extend(new_tweets)

        # updating the id of the oldest tweet less one
        oldest = alltweets[-1].id - 1

        # getting the extracted count
        print("...%s tweets downloaded so far" % (len(alltweets)))

    # writing the extracted tweets to csv file
    print ("Writing tweets to CSV please wait...")

    # creating a dictionary to save tweets along with timestamp
    data = {'Tweet_Text': [],'Timestamp':[]}

    # iterating over all of the extracted tweets
    for status in alltweets:
        # replacing newlines in a tweet
        tweet = re.sub('\n',' ',status._json['text'])
        # adding data to the dictionary
        data['Tweet_Text'].append(tweet)
        data['Timestamp'].append(status._json['created_at'])

    # creating a dataframe
    df = pd.DataFrame(data)
    print(df.to_string)

    # writing dataframe to a csv file
    df.to_csv('Cricketworldcup.csv', encoding='utf-8', index=False)

    print("Done...!!!")

if __name__ == '__main__':
    # pass in the username of the account (Event Page) you want to download
    get_all_tweets("@Cricketworldcup")