
#Data Preprocessing


# importing required libraries

import logging
import re
import itertools
import json
import csv
from autocorrect import spell
import pandas as pd
from wordsegment import load, segment
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import stopwords
from html.parser import HTMLParser
logging.basicConfig(level=logging.INFO)

# preprocessing the input data
def preprocess(fp):

    corpus = []
    load()
    stopWords = set(stopwords.words('english'))
    data = {'Tweet_Text': [] ,'Timestamp':[] };

    with open(fp, 'rt', encoding='utf-8') as data_in:
        csv_reader = csv.reader(data_in, delimiter=',')
        for line in csv_reader:
            # discarding first line
            if not 'Timestamp' in line[0]:

                # dividing a row into two sections and selectig text for further processing
                timestamp = line [0]
                tweet = line[1]
                tweet = tweet.strip()
                # remove retweets tag
                tweet = re.sub(r'RT','', tweet)

                 # remove url
                tweet = re.sub(r'(http|https?|ftp)://[^\s/$.?#].[^\s]*', '', tweet, flags=re.MULTILINE)
                tweet = re.sub(r'[http?|https?]:\\/\\/[^\s/$.?#].[^\s]*', '', tweet, flags=re.MULTILINE)

                # remove mentions
                remove_mentions = re.compile(r'(?:@[\w_]+)')
                tweet = remove_mentions.sub('',tweet)

                # remove emoticons
                try:
                    emoji_pattern = re.compile("["
                        u"\U0001F600-\U0001F64F" # emoticons
                        u"\U0001F650-\U0001F67F" # ornamental dingbats
                        u"\U0001F300-\U0001F5FF" # symbols & pictographs
                        u"\U0001F680-\U0001F6FF" # transport & map symbols
                        u"\u2600-\u26FF\u2700-\u27BF" # mislaneous
                        u"\U00002702-\U000027B0" 
                        u"\U000024C2-\U0001F251"
                        u"\U0001F1E0-\U0001F1FF" # flags (iOS)
                        "]+", flags=re.UNICODE)
                    tweet = emoji_pattern.sub(r'', tweet)
                except Exception:
                    pass


                # remove unicode
                try:
                    tweet = tweet.encode('ascii').decode('unicode-escape').encode('ascii','ignore').decode("utf-8")
                except Exception:
                    pass

                # remove more unicode characters
                try:
                    tweet = tweet.encode('ascii').decode('unicode-escape')
                    tweet = HTMLParser().unescape(tweet)
                except Exception:
                    pass

                # Standardising words
                tweet = ''.join(''.join(s)[:2] for _, s in itertools.groupby(tweet))

                # applying contractions
                words = tweet.split()
                tweet = [apoDict[word] if word in apoDict else word for word in words]
                tweet = " ".join(tweet)

                hashWords =  re.findall(r'(?:^|\s)#{1}(\w+)', tweet)
                # replace #word with word
                tweet = re.sub(r'(?:^|\s)#{1}(\w+)', r' \1', tweet)

                # word segmentation
                token_list =  word_tokenize(tweet)
                segmented_word = []
                for i in token_list:
                    if i in hashWords:
                        seg = segment(i)
                        segmented_word.extend(seg)
                    else:
                        segmented_word.append(i)

                tweet = ' '.join(segmented_word)

                # remove special symbols
                tweet = re.sub('[@#$._|]', ' ', tweet)

                # remove extra whitespaces
                tweet = re.sub('[\s]+', ' ', tweet)

                # Spelling correction
                spell_list = word_tokenize(tweet)
                filterlist = []
                for i in spell_list:
                    correct_spell = spell(i)
                    filterlist.append(correct_spell)
                tweet = ' '.join(filterlist)

                # lemmatization of tweets
                tweet = word_tokenize(tweet)
                lemma_tweet = []
                for tweet_word in tweet:
                    lemma_tweet.append(WordNetLemmatizer().lemmatize(tweet_word,'v'))

                tweet = ' '.join(lemma_tweet)

                # remove stop words
                token_list = word_tokenize(tweet)
                wordsFiltered = []
                for i in token_list:
                    if i not in stopWords:
                        wordsFiltered.append(i)
                tweet = ' '.join(wordsFiltered)

                # remove open or empty lines
                if not re.match(r'^\s*$', tweet):
                    if not len(tweet) <= 3:
                        corpus.append(tweet)

                # creating a dictionary
                data['Tweet_Text'].append(tweet)
                data['Timestamp'].append(timestamp)
    #creating a dataframe
    df = pd.DataFrame(data)
    print(df.to_string)

    #to order the columns in the csv file (while copying from dataframes to a CSV file)
    df_reorder = df[['Timestamp','Tweet_Text']]

    #writing dataframe to a csv file
    df_reorder.to_csv('/home/stark/PycharmProjects/tmp_selr/Preprocessing/Preprocessed/Cricket_pre.csv',encoding='utf-8', index= False)

    return corpus

# Contractions with the
def loadAppostophesDict(fp_contraction):
    apoDict = json.load(open(fp_contraction))
    return apoDict

if __name__ == "__main__":

    # function calls to contractions and preprocessing
    DATASET_FP = "/home/stark/PycharmProjects/tmp_selr/TweetsDownload/Cricketworldcup.csv"
    APPOSTOPHES_FP = "/home/stark/PycharmProjects/tmp_selr/Preprocessing/appos_dict.txt"
    apoDict = loadAppostophesDict(APPOSTOPHES_FP)
    corpus = preprocess(DATASET_FP)