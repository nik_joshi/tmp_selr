#importing csv package to process

import csv

def duplicateRemoval():
    # creating two lists one for existing tweets and another one to store unique onces
    tweetChecklist = []
    list = []
    with open('/home/stark/PycharmProjects/tmp_selr/TweetsDownload/raw_csv/Sports.csv', 'r', encoding='utf-8')as fh:
        #adding all tweets in a list
        for line in fh:
            list.append(line.strip().split('\n'))

    #iterating over the tweet list and removing duplicate ones
    for current_tweet in list:
        # If tweet doesn't exist in the list
        if current_tweet not in tweetChecklist:
            tweetChecklist.append(current_tweet);
            print(current_tweet)


    print(tweetChecklist)

    #writing tweets back to the csv
    csvfileWrite = '/home/stark/PycharmProjects/tmp_selr/TweetsDownload/raw_csv/Sports_Unique.csv'
    with open(csvfileWrite, "w") as output:
        writer = csv.writer(output, lineterminator='\n')
        writer.writerows(tweetChecklist)
    print("Done removing duplicates !")

if __name__ == "__main__":
    # function call to remove duplicate tweets
    duplicateRemoval()
