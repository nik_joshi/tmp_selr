#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct  8 17:30:45 2018

@author: stark
"""

import pandas as pd
import spacy
import numpy as np
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score
import matplotlib.pyplot as plt
from sklearn.naive_bayes import GaussianNB

def createFeatureVectorAndApplyClassifier():

    nlp = spacy.load('en', vectors='en_glove_cc_300_1m_vectors')
    df = pd.read_csv("/home/stark/PycharmProjects/tmp_selr/NER/finRep/combined_shuf.csv", index_col=False)
    df = df.sample(frac=1).reset_index(drop=True)

    # df.to_csv('/home/stark/PycharmProjects/tmp_selr/NER/replaced/shuffled.csv',encoding='utf-8',index= False)
    # python -m spacy download en

    tweet_text = df['Tweet_Text'].tolist()
    y = df['tag'].tolist()
    m_length = len(tweet_text)
    X_Shape = (m_length, 384)
    X = np.zeros(X_Shape)

    for i, message in enumerate(tweet_text):
        # Pass each each sentence to the nlp object to create a document
        # doc = nlp(unicode(message))
        #nlp.maxl_length = 700000
        doc = nlp(message)
        # Save the document's .vector attribute to the corresponding row in X
        X[i, :] = doc.vector

    # dividing X, y into train and test data
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state=0)

    # training a DescisionTreeClassifier

    dtree_model = DecisionTreeClassifier(max_depth=2).fit(X_train, y_train)
    dtree_predictions = dtree_model.predict(X_test)

    accuracy_dt = accuracy_score(y_test, dtree_predictions)
    cm_dt = confusion_matrix(y_test, dtree_predictions)

    # training svm
    svm_model_linear = SVC(kernel='linear', C=1).fit(X_train, y_train)
    svm_predictions = svm_model_linear.predict(X_test)
    # model accuracy for X_test
    accuracy_svm = svm_model_linear.score(X_test, y_test)

    # creating a confusion matrix
    cm_svm = confusion_matrix(y_test, svm_predictions)

    # training a Naive Bayes classifier


    gnb = GaussianNB().fit(X_train, y_train)
    gnb_predictions = gnb.predict(X_test)

    # accuracy on X_test
    accuracy_nb = gnb.score(X_test, y_test)
    print (accuracy_nb)

    # creating a confusion matrix
    cm_nb = confusion_matrix(y_test, gnb_predictions)


    #plotting bar chart of accuracies
    X_values = ['DT','SVM','NB']
    y_values = [accuracy_dt,accuracy_svm,accuracy_nb]
    width = 1/1.5

    plt.bar(X_values, y_values,width, color="blue")




if __name__ == "__main__":
    # function calls to parse NERD
    createFeatureVectorAndApplyClassifier()
